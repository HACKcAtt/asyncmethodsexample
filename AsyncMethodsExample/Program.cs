﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncMethodsExample
{
    class Program
    {
        static void Main(string[] args)
        {
            // .GetAwaiter() остановит выполнение в вызывающем потоке, пока задача не завершится, .GetResult() вернёт результат.
            DisplayResultAsync().GetAwaiter().GetResult();
            DisplayResultAsyncWithTaskWhenAll().GetAwaiter().GetResult();
            DisplayResultAsyncWithCancel(6);
            Console.WriteLine("Задача DisplayResultAsync завершена");
            Console.ReadLine();
        }
        // Ключевое слово async указывает, что метод или лямбда-выражение являются асинхронными.
        // А оператор await применяется к задаче в асинхронных методах, чтобы приостановить выполнение
        // метода до тех пор, пока эта задача не завершится.При этом выполнение потока, в котором был
        // вызван асинхронный метод, не прерывается.
        static async Task DisplayResultAsync()
        {
            int num = 5;
            int result = await FactorialAsync(num);
            Console.WriteLine("Обычный асинхронный метод. Факториал числа {0} равен {1}", num, result);

            // Когда система встречает в блоке кода оператор await или вызов метода GetAwaiter, то выполнение
            // в вызывающем потоке останавливается, пока не завершится вызываемая задача. После завершения задачи
            // управление переходит к следующему оператору await и снова метод DisplayResultAsync ждет, когда
            // завершится задача. Это позволяет вызывать задачи последовательно в определнном порядке, что бывает
            // необходимо, если одна задача зависит от результатов другой.

            num = 6;
            result = FactorialAsync(num).GetAwaiter().GetResult();
            Console.WriteLine("Обычный асинхронный метод, используется GetAwaiter. Факториал числа {0} равен {1}", num, result);

            result = await Task.Run(() =>
            {
                int res = 1;
                for (int i = 1; i <= 9; i++)
                {
                    res += i * i;
                }
                return res;
            });
            Console.WriteLine("Обычный асинхронный метод. Сумма квадратов чисел равна {0}", result);
        }

        // Однако не всегда существует подобная зависимость между задачами.В этом случае мы можем 
        // запустить все задачи одновременно через метод Task.WhenAll.
        static async Task DisplayResultAsyncWithTaskWhenAll()
        {
            int num1 = 5;
            int num2 = 6;
            Task<int> t1 = FactorialAsync(num1);
            Task<int> t2 = FactorialAsync(num2);
            Task<int> t3 = Task.Run(() =>
            {
                int res = 1;
                for (int i = 1; i <= 9; i++)
                {
                    res += i * i;
                }
                return res;
            });

            await Task.WhenAll(new[] { t1, t2, t3 });
            // Task.WhenAll создает новую задачу, которая будет автоматически выполнена после выполнения всех
            // предоставленных задач, то есть задач t1, t2, t3. А с помощью оператора await ожидаем ее завершения.
            // В итоге все три задачи теперь будут запускаться параллельно, однако вызывающий метод DisplayResultAsync
            // благодаря оператору await все равно будет ожидать, пока они все не завершатся.И если задача возвращает
            // какое - нибудь значение, то это значение потом можно получить с помощью свойства Result.

            Console.WriteLine("Асинхронный метод с использованием Task.WhenAll. Факториал числа {0} равен {1}", num1, t1.Result);
            Console.WriteLine("Асинхронный метод с использованием Task.WhenAll. Факториал числа {0} равен {1}", num2, t2.Result);
            Console.WriteLine("Асинхронный метод с использованием Task.WhenAll. Сумма квадратов чисел равна {0}", t3.Result);
        }

        // Для отмены асинхронных операций используются классы CancellationToken и CancellationTokenSource.
        // Для создания токена определяется объект CancellationTokenSource. Метод FactorialAsync в качестве
        // параметра принимает токен, и если где-то во внешнем коде произойдет отмена операции через вызов
        // cts.Cancel, то в методе FactorialAsync выражение token.ThrowIfCancellationRequested() генерирует
        // исключение OperationCanceledException. Это исключение затем перехватывается во внешнем коде в блоке try..catch.
        static async void DisplayResultAsyncWithCancel(int num)
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            try
            {
                Task t1 = FactorialAsyncWithCancel(num, cts.Token);
                Task t2 = Task.Run(() =>
                {
                    Thread.Sleep(2000);
                    // Отмена асинхронной операции.
                    cts.Cancel(); 
                });
                await Task.WhenAll(t1, t2);
            }
            catch (OperationCanceledException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                cts.Dispose();
            }
        }

        static Task FactorialAsyncWithCancel(int x, CancellationToken token)
        {
            return Task.Run(() =>
            {
                int result = 1;
                for (int i = 1; i <= x; i++)
                {
                    token.ThrowIfCancellationRequested();
                    result *= i;
                    Console.WriteLine("Асинхронный метод с использованием отмены асинхронной операции (классы CancellationToken и CancellationTokenSource). Факториал числа {0} равен {1}", i, result);
                    Thread.Sleep(1000);
                }
            }, token);
        }

        // При определении асинхронных методов надо учитывать, что в качестве возвращаемого результата должен быть
        // объект Task<T> или просто Task (если метод не возвращает результата, как DisplayResultAsync) или void.
        static async Task<int> FactorialAsync(int x)
        {
            int result = 1;
            // Покуда задача выполняется, выполнение метода не идёт.
            return await Task.Run(() =>
            {
                for (int i = 1; i <= x; i++)
                {
                    result *= i;
                }
                return result;
            });
        }
    }
}